package com.ping.me;

import android.annotation.SuppressLint;
import android.net.Uri;
import android.os.Build;
import android.provider.ContactsContract.Contacts;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

public class Settings {

	
	  
	    /*
	     * Defines an array that contains column names to move from
	     * the Cursor to the ListView.
	     */
	    @SuppressLint("InlinedApi")
	    public final static String[] FROM_COLUMNS = {
	            Build.VERSION.SDK_INT
	                    >= Build.VERSION_CODES.HONEYCOMB ?
	                    Contacts.DISPLAY_NAME_PRIMARY :
	                    Contacts.DISPLAY_NAME
	    };
	    /*
	     * Defines an array that contains resource ids for the layout views
	     * that get the Cursor column contents. The id is pre-defined in
	     * the Android framework, so it is prefaced with "android.R.id"
	     */
	    public final static int[] TO_IDS = {
	           android.R.id.text1
	    };
	    // Define global mutable variables
	    // Define a ListView object
	    public static ListView mContactsList;
	    // Define variables for the contact the user selects
	    // The contact's _ID value
	    public static long mContactId;
	    // The contact's LOOKUP_KEY
	    public static String mContactKey;
	    // A content URI for the selected contact
	    public static Uri mContactUri;
	    // An adapter that binds the result Cursor to the ListView
	    public static SimpleCursorAdapter mCursorAdapter;
}
