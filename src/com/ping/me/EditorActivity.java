package com.ping.me;

import java.util.Calendar;
import java.util.Date;

import com.parse.ParseObject;

import android.app.Activity;
import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TimePicker;
import android.os.Build;

public class EditorActivity extends Activity {
	
	String targetNumber;
	String targetName;
	Date targetDate;
	ParseObject testObject;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_editor);
		
		Intent intent =  this.getIntent();
		
		targetNumber = intent.getStringExtra("mobileNum");
		targetName = intent.getStringExtra("mobileName");
		

		if (savedInstanceState == null) {
			getFragmentManager().beginTransaction()
					.add(R.id.container, new PlaceholderFragment()).commit();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.editor, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		} 

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_editor,
					container, false);
			return rootView;
		}
	}
	
	
	public void clickBack(View v)
	{
		finish();
	}
	
	public void sendPing(View v)
	{
		EditText et =  (EditText) findViewById(R.id.editText1);
		TimePicker tp = (TimePicker) findViewById(R.id.timePicker1);
		
		
 
		final Calendar c = Calendar.getInstance();
		
		int hour = tp.getCurrentHour();
		int minute = tp.getCurrentMinute();
		
		
		c.set(Calendar.HOUR_OF_DAY, hour);
		c.set(Calendar.MINUTE, minute);
		
		Date d = c.getTime();
		
		Log.i("ping.me", et.getText().toString());
		Log.i("ping.me", d.toString());
		
		targetDate = d;
		
		
		InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.toggleSoftInput (InputMethodManager.SHOW_FORCED, 0);
		
		push(et.getText().toString());
		
		new AlertDialog.Builder(this)
	    .setTitle("Ping.me -> "+targetName)
	    .setMessage("Сообщение для "+targetName+" отправлено")
	    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
	        public void onClick(DialogInterface dialog, int which) { 
	            // continue with delete
	        	 
	        	clickBack(null);
	        }
	     })	    
	    .setIcon(android.R.drawable.ic_dialog_alert)
	     .show();
		
	}
	
	
	@SuppressWarnings("deprecation")
	public void push(String txt) 
    {
    	/*
    	ParseQuery q = ParseInstallation.getQuery();
    	q.whereContains("channels", "pingme");
    	*/
    	/*
        push = new ParsePush();
       // push.setPushToAndroid(true);
        push.setChannel("pingme");
        push.setMessage("sparta");
        push.sendInBackground(); */
    	
		//myMobile = et.getText().toString(); 
    	targetNumber = 'a'+targetNumber.replace(" ", "");   
    	
    	  testObject = new ParseObject("messages"); 
          testObject.put("txt", txt);
          testObject.put("channel", targetNumber); 
 
          //Date c = Calendar.();
          /*
          Calendar cal = Calendar.getInstance();
          cal.setTime(new Date()); // sets calendar time/date
          cal.add(Calendar.HOUR_OF_DAY, 1); // adds one hour
          Date d2 = cal.getTime(); // returns new date object, one hour in the future
			*/
          
          testObject.put("pushDate", targetDate);
          testObject.saveInBackground();
    }

}
