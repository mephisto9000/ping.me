package com.ping.me;

import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import com.ping.me.R;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.parse.FindCallback;
import com.parse.Parse;
import com.parse.ParseAnalytics;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParsePush;
import com.parse.ParseQuery;
import com.parse.PushService;

import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import com.parse.Parse;
import com.parse.ParseAnalytics;
import com.parse.PushService;





public class MainActivity extends Activity {

	ParsePush push;
	String myMobile;
	
	MainActivity self;
	
	 final List<String> phones = new LinkedList<String>();
     final List<String> names = new LinkedList<String>(); 
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
         
    	super.onCreate(savedInstanceState);
    	
    	
        self = this;
        
        
        PushService.setDefaultPushCallback(this, MainActivity.class);
        
        ParseAnalytics.trackAppOpened(getIntent());
        
        setContentView(R.layout.activity_main);
        
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        
        /*
        ParseObject testObject = new ParseObject("TestObject");
        testObject.put("foo", "bar");
        testObject.saveInBackground(); */


        /*
        ParseQuery<ParseObject> query = ParseQuery.getQuery("TestObject");
        query.whereEqualTo("foo", "bar");
        query.findInBackground(new FindCallback<ParseObject>() {
        	@Override
            public void done(List<ParseObject> scoreList, ParseException e) {
                if (e == null) {
                    Log.i("score", "Retrieved " + scoreList.size() + " scores");
                } else {
                    Log.i("score", "Error: " + e.getMessage());
                }
            }

        });*/
        
       
        
        //final List<String> phones = new LinkedList<String>();
        //final List<String> names = new LinkedList<String>();
        
        Context context = getApplicationContext(); 
        Cursor cursor = context.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, null);
        while(cursor.moveToNext()) 
        {
            int phone_idx = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
            int name_idx = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
            String phone = cursor.getString(phone_idx);
            String name = cursor.getString(name_idx);
            
            phones.add(phone);
            names.add(name);
            //result.put(name, phone);
            
            Log.i("ping.me", "phone = "+phone);
        }
        
       /*
        ListAdapter adapter = new SimpleCursorAdapter(this, 
        											android.R.layout.simple_list_item_2, 
        											cursor,
        											names,
        											phones); */
        
        ArrayAdapter adapter = new ArrayAdapter(context, android.R.layout.simple_list_item_2, android.R.id.text2, names) {
        	  @Override
        	  public View getView(int position, View convertView, ViewGroup parent) {
        	    View view = super.getView(position, convertView, parent);
        	    TextView text1 = (TextView) view.findViewById(android.R.id.text1);
        	    TextView text2 = (TextView) view.findViewById(android.R.id.text2);

        	    text1.setText(names.get(position));
        	    
        	    text1.setTextColor(R.color.PingBlack);
        	      
        	    text2.setText(phones.get(position));
        	    
        	    text2.setTextColor(R.color.PingGray);
        	    
        	   // text1.setBackgroundColor(R.color.black);
        	    return view;
        	  }
        	};

        ListView contacts = (ListView)findViewById(R.id.listView1);
        
        contacts.setAdapter(adapter);
        
        contacts.setOnItemClickListener(new OnItemClickListener() {
                  
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				  Log.i("ping.me", "itemSelect: position = " + arg2 + ", id = "
		                  + arg3);
				  
				  jumpToEditor(arg2);
			}
          });
        
        
        TelephonyManager tm = (TelephonyManager)getSystemService(TELEPHONY_SERVICE); 
        String number = tm.getLine1Number();
        
      
        
        if (number != null)        
        	Log.i("ping.me", "phone == "+ number);
        else
        	Log.i("ping.me", "mobile == null");
        
        myMobile = number;
        
        EditText et = (EditText) findViewById(R.id.editText1);
        
        if (myMobile!=null)
        et.setText(myMobile);
        
        myMobile = 'a'+myMobile;
        
        //myMobile = myMobile.replace("+", "");
        //myMobile = myMobile.trim();
        
        
        Log.i("ping.me", "mobile ==" + myMobile);
        
        //LinkedList<String> channels = new LinkedList<String>();
        //channels.add("pingme");
        //channels.add("Mets"); 
        
         
        PushService.subscribe(context, myMobile, MainActivity.class);
       // PushService.setDefaultPushCallback(context, YourActivity.class);
        
        Intent intent = this.getIntent();
        intent.addFlags(32);
        
        /*
        ParsePush push = new ParsePush();
        push.setChannels(channels); // Notice we use setChannels not setChannel
        push.setMessage("The Giants won against the Mets 2-3.");
        push.sendInBackground(); */
         
    }
     
    public void save(View v)
    {
    	
    	EditText et = (EditText) findViewById(R.id.editText1);
         
    	myMobile = et.getText().toString();
    	myMobile = myMobile.replace(" ", "");
    	
    	
        //if (myMobile!=null)
        // et.setText(myMobile);
          
        myMobile = 'a'+myMobile;
    	PushService.subscribe(self.getApplicationContext(), myMobile, MainActivity.class);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    
    


    public void jumpToEditor(int itemNum)
    {
    	Intent i = new Intent(this, EditorActivity.class);
    	i.putExtra("mobileNum", phones.get(itemNum));
    	i.putExtra("mobileName", names.get(itemNum));
    	
    	startActivity(i);
    }
    
}
